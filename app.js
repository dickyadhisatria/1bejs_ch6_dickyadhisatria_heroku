const express = require("express");
const router = require("./routes/indexRoute");
const app = express();
const port = 3000;
const morgan = require("morgan");
const cors = require("cors");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./api-doc/swagger-output.json");

app.use("/dickyadhisatria", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(helmet());
app.use(cors());

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", router);

// app.listen(port, () => {
//   console.log(`Server is running on port ${port}`);
// });

module.exports = app;
