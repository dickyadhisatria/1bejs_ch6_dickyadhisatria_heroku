"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "channels",
      [
        {
          user_id: 1,
          channel_name: "Programming",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          user_id: 2,
          channel_name: "CTO",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
