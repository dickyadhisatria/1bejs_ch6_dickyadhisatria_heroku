const app = require("../app");
const request = require("supertest");
const { sequelize } = require("../models");
const bcrypt = require("bcrypt");

beforeAll(async () => {
  try {
    await sequelize.sync();
    await sequelize.queryInterface.bulkInsert(
      "users",
      [
        {
          username: "dickyadhisatria",
          password: bcrypt.hashSync("dicky2uxgg", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "mugeki",
          password: bcrypt.hashSync("root", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "testing",
          password: bcrypt.hashSync("testing", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "testing2",
          password: bcrypt.hashSync("testing2", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
    await sequelize.queryInterface.bulkInsert(
      "channels",
      [
        {
          user_id: 1,
          channel_name: "Programming",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          user_id: 2,
          channel_name: "CTO",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
    await sequelize.queryInterface.bulkInsert(
      "videos",
      [
        {
          channel_id: 1,
          title: "How to use Node.js",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          channel_id: 2,
          title: "How to use React.js",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  } catch (err) {
    await sequelize.sync({ force: true });
    await sequelize.close();
  }
}, 10000);

afterAll(async () => {
  await sequelize.sync({ force: true });
  await sequelize.close();
}, 10000);

describe("UserController", () => {
  test("SUCCESS, create user", async () => {
    const data = {
      username: "testing3",
      password: "testing3",
    };
    const response = await request(app).post("/user/register").send(data);
    expect(response.status).toBe(201);
  });
  test("FAILED, create user because username has taken", async () => {
    const data = {
      username: "testing2",
      password: "testing2",
    };
    const response = await request(app).post("/user/register").send(data);
    expect(response.status).toBe(400);
  });
  test("SUCCESS, login user", async () => {
    const data = {
      username: "testing",
      password: "testing",
    };
    const response = await request(app).post("/user/login").send(data);
    expect(response.status).toBe(200);
  });
  test("FAILED, login user because user not found", async () => {
    const data = {
      username: "wrongusername",
      password: "wrongpassword",
    };
    const response = await request(app).post("/user/login").send(data);
    expect(response.status).toBe(404);
  });
  test("SUCCESS, get all users", async () => {
    const response = await request(app).get("/user/users");
    expect(response.status).toBe(200);
  });
  test("FAILED, delete user because user not found", async () => {
    const response = await request(app).delete("/user/delete/69");
    expect(response.status).toBe(404);
  });
});

describe("ChannelController", () => {
  test("SUCCESS, create channel", async () => {
    const data = {
      channel_name: "testing_channel",
      user_id: 3,
    };
    const response = await request(app).post("/channel/create").send(data);
    expect(response.status).toBe(201);
  });
  test("FAILED, create channel because channel name has taken", async () => {
    const data = {
      channel_name: "CTO",
      user_id: 2,
    };
    const response = await request(app).post("/channel/create").send(data);
    expect(response.status).toBe(400);
  });
  test("SUCCESS, get all channels", async () => {
    const response = await request(app).get("/channel/all");
    expect(response.status).toBe(200);
  });
  test("SUCCESS, get channel by id", async () => {
    const response = await request(app).get("/channel/get/3");
    expect(response.status).toBe(200);
  });
  test("FAILED, get channel by id because channel not found", async () => {
    const response = await request(app).get("/channel/get/69");
    expect(response.status).toBe(404);
  });
  test("SUCCESS, delete channel by user id", async () => {
    const response = await request(app).delete("/channel/deleteByUserId/3");
    expect(response.status).toBe(204);
  });
  test("FAILED, delete channel by user id because channel not found", async () => {
    const response = await request(app).delete("/channel/deleteByUserId/69");
    expect(response.status).toBe(404);
  });
});

describe("VideoController", () => {
  test("SUCCESS, create video", async () => {
    const data = {
      title: "How to use Success in Binar",
      channel_id: 1,
    };
    const response = await request(app).post("/video/create").send(data);
    expect(response.status).toBe(201);
  });
  test("FAILED, create video because channel not found", async () => {
    const data = {
      title: "How to use Failed in Binar",
      channel_id: 69,
    };
    const response = await request(app).post("/video/create").send(data);
    expect(response.status).toBe(404);
  });
  test("SUCCESS, get all videos", async () => {
    const response = await request(app).get("/video/all");
    expect(response.status).toBe(200);
  });
  test("SUCCESS, get video by id", async () => {
    const response = await request(app).get("/video/get/1");
    expect(response.status).toBe(200);
  });
  test("FAILED, get video by id because video not found", async () => {
    const response = await request(app).get("/video/get/69");
    expect(response.status).toBe(404);
  });
  test("SUCCESS, delete video by id", async () => {
    const response = await request(app).delete("/video/deleteById/3");
    expect(response.status).toBe(204);
  });
  test("FAILED, delete video by id because video not found", async () => {
    const response = await request(app).delete("/video/deleteById/69");
    expect(response.status).toBe(404);
  });
});
