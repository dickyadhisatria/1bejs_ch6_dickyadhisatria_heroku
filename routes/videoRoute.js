const express = require("express");
const router = express.Router();
const {
  createNewVideo,
  getAllVideos,
  getVideo,
  getVideoByChannel,
  deleteVideoById,
} = require("../controllers/videoController");

router.post("/create", createNewVideo);
router.get("/all", getAllVideos);
router.get("/get/:id", getVideo);
router.get("/getByChannel/:channel_id", getVideoByChannel);
router.delete("/deleteById/:id", deleteVideoById);

module.exports = router;
