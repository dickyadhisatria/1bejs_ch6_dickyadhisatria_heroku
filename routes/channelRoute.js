const express = require("express");
const router = express.Router();
const {
  createChannel,
  getAllChannels,
  getChannel,
  deleteChannelByUserId,
} = require("../controllers/channelController");

router.post("/create", createChannel);
router.get("/all", getAllChannels);
router.get("/get/:id", getChannel);
router.delete("/deleteByUserId/:id", deleteChannelByUserId);

module.exports = router;
