const { channel: Channel } = require("../models");
const { user: User } = require("../models");

const createChannel = async (req, res) => {
  const { channel_name, user_id } = req.body;
  await User.findOne({
    where: {
      id: user_id,
    },
  }).then((user) => {
    if (!user) {
      return res.status(404).json({
        error: "User not found",
      });
    } else {
      Channel.findOne({
        where: {
          channel_name: channel_name,
        },
      }).then((channel) => {
        if (channel) {
          return res.status(400).json({
            error: "Channel already exists",
          });
        } else {
          Channel.create({
            user_id: user_id,
            channel_name: channel_name,
          }).then((channel) => {
            return res.status(201).json({
              message: "Channel created",
              channel: channel,
            });
          });
        }
      });
    }
  });
};

const getAllChannels = async (req, res) => {
  try {
    const channels = await Channel.findAll({
      attributes: ["id", "channel_name"],
    });
    return res
      .status(200)
      .json({ message: "Channels found", channels: channels });
  } catch (err) {
    return res.status(400).json(err);
  }
};

const getChannel = async (req, res) => {
  const { id } = req.params;
  await Channel.findOne({
    where: {
      id: id,
    },
  }).then((channel) => {
    if (!channel) {
      return res.status(404).json({
        error: "Channel not found",
      });
    } else {
      return res.status(200).json({
        message: "Channel found",
        channel: channel,
      });
    }
  });
};

const deleteChannelByUserId = async (req, res) => {
  const { id } = req.params;
  await User.findOne({
    where: {
      id: id,
    },
  }).then((user) => {
    if (!user) {
      return res.status(404).json({
        error: "User not found",
      });
    } else {
      Channel.findOne({
        where: {
          user_id: user.id,
        },
      }).then((channel) => {
        if (!channel) {
          return res.status(404).json({
            error: "Channel not found",
          });
        } else {
          channel.destroy().then((channel) => {
            return res.status(204).json({
              message: "Channel deleted",
              channel: channel,
            });
          });
        }
      });
    }
  });
};

module.exports = {
  createChannel,
  getAllChannels,
  getChannel,
  deleteChannelByUserId,
};
