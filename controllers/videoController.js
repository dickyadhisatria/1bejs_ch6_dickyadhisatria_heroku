const { video: Video } = require("../models");
const { channel: Channel } = require("../models");

const createNewVideo = async (req, res) => {
  const { title, channel_id } = req.body;
  await Channel.findOne({
    where: {
      id: channel_id,
    },
  }).then((channel) => {
    if (!channel) {
      return res.status(404).json({
        error: "Channel not found",
      });
    } else {
      Video.findOne({
        where: {
          title: title,
        },
      }).then((video) => {
        if (video) {
          return res.status(400).json({
            error: "Video title has taken",
          });
        } else {
          Video.create({
            title: title,
            channel_id: channel_id,
          }).then((video) => {
            return res.status(201).json({
              message: "Video created",
              video: video,
            });
          });
        }
      });
    }
  });
};

const getAllVideos = async (req, res) => {
  try {
    const videos = await Video.findAll({
      attributes: ["id", "title", "createdAt", "updatedAt"],
    });
    return res.status(200).json({ message: "Videos found", videos: videos });
  } catch (err) {
    return res.status(400).json(err);
  }
};

const getVideo = async (req, res) => {
  const { id } = req.params;
  await Video.findOne({
    where: {
      id: id,
    },
  }).then((video) => {
    if (!video) {
      return res.status(404).json({
        error: "Video not found",
      });
    } else {
      return res.status(200).json({
        message: "Video found",
        video: video,
      });
    }
  });
};

const getVideoByChannel = async (req, res) => {
  const { channel_id } = req.params;
  await Video.findAll({
    where: {
      channel_id: channel_id,
    },
  }).then((videos) => {
    if (!videos) {
      return res.status(404).json({
        error: "Videos not found",
      });
    } else {
      return res.status(200).json({
        message: "Videos found",
        videos: videos,
      });
    }
  });
};

const deleteVideoById = async (req, res) => {
  const { id } = req.params;
  await Video.findOne({
    where: {
      id: id,
    },
  }).then((video) => {
    if (!video) {
      return res.status(404).json({
        error: "Video not found",
      });
    } else {
      Video.destroy({
        where: {
          id: id,
        },
      }).then((video) => {
        return res.status(204).json({
          message: "Video deleted",
          video: video,
        });
      });
    }
  });
};

module.exports = {
  createNewVideo,
  getAllVideos,
  getVideo,
  getVideoByChannel,
  deleteVideoById,
};
