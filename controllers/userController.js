const { user: User } = require("../models");
const bcrypt = require("bcrypt");

const createUser = async (req, res) => {
  const { username, password } = req.body;
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);
  await User.findOne({
    where: {
      username: username,
    },
  }).then((user) => {
    if (user) {
      return res.status(400).json({
        error: "Username already taken",
      });
    } else {
      const user = new User({
        username: username,
        password: hashedPassword,
      });
      user.save().then((user) => {
        console.log("RES", res);
        return res.status(201).json({
          message: "User created successfully",
          user: user,
        });
      });
    }
  });
};

const getAllUsers = async (req, res) => {
  try {
    const users = await User.findAll({
      attributes: ["id", "username"],
    });
    return res.status(200).json({ message: "Users found", users: users });
  } catch (err) {
    return res.status(400).json(err);
  }
};

const loginUser = async (req, res) => {
  const { username, password } = req.body;
  await User.findOne({
    where: {
      username: username,
    },
  }).then((user) => {
    if (!user) {
      return res.status(404).json({
        error: "User not found",
      });
    } else {
      bcrypt.compare(password, user.password, (err, result) => {
        if (result) {
          return res.status(200).json({
            message: "Login successful",
            user: user,
          });
        } else {
          return res.status(400).json({
            error: "Password is incorrect",
          });
        }
      });
    }
  });
};

const deleteUser = async (req, res) => {
  await User.findOne({
    where: {
      id: req.params.id,
    },
  }).then((user) => {
    if (!user) {
      return res.status(404).json({
        error: "User not found",
      });
    } else {
      user.destroy({ where: { id: req.params.id } }).then((user) => {
        User.findAll({
          attributes: ["id", "username"],
        }).then((users) => {
          return res
            .status(204)
            .json({ message: "User deleted", users: users });
        });
      });
    }
  });
};

module.exports = {
  createUser,
  getAllUsers,
  loginUser,
  deleteUser,
};
