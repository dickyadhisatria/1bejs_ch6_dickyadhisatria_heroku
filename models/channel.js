"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class channel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      channel.belongsTo(models.user, {
        foreignKey: "user_id",
        as: "user",
        onDelete: "CASCADE",
      });
      channel.hasMany(models.video, {
        foreignKey: "channel_id",
        as: "video",
        onDelete: "CASCADE",
      });
    }
  }
  channel.init(
    {
      user_id: DataTypes.INTEGER,
      channel_name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "channel",
    }
  );
  return channel;
};
